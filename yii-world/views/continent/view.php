<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model app\models\Continent */

$this->title = $continent->name;
$this->params['breadcrumbs'][] = ['label' => 'Continents', 'url' => ['list']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="continent-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $continent->continent_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $continent->continent_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?= DetailView::widget([
        'model' => $continent,
        'attributes' => [
            'continent_id',
            'code',
            'name',
            'description:ntext',
        ],
        ]) ?>

<?= ListView::widget([
    'dataProvider' => $countriesProvider,
    'itemView' => '_country',]);
    ?>

</div>
